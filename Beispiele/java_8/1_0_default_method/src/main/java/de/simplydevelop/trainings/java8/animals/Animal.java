package de.simplydevelop.trainings.java8.animals;


/**
 * Created by goetzingert on 11.03.16.
 */
public interface Animal {

    int getWeight();

    void setWeight(int weight);


    default void eat()
    {
        setWeight(getWeight() + 1);
    }
}
