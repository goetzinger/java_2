/**
 * Copyright (c) Deere & Company as an unpublished work
 * THIS SOFTWARE AND/OR MATERIAL IS THE PROPERTY OF
 * DEERE & COMPANY.  ALL USE, DISCLOSURE, AND/OR
 * REPRODUCTION NOT SPECIFICALLY AUTHORIZED BY
 * DEERE & COMPANY IS PROHIBITED.
 */
package de.simplydevelop.trainings.java8.animals;

/**
 * Created by goetzingert on 11.03.16.
 */
public class Schwein implements Animal {

    private int weight;
    @Override
    public int getWeight() {
        return weight;
    }

    @Override
    public void setWeight(int weight) {
        this.weight = weight;
    }
}
