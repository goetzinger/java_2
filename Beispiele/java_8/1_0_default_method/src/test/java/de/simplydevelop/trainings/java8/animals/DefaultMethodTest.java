package de.simplydevelop.trainings.java8.animals;


import org.junit.Assert;
import org.junit.Test;

/**
 * Created by goetzingert on 11.03.16.
 */
public class DefaultMethodTest {


    @Test
    public void testDefault() {
        Schwein missPiggy = new Schwein();
        missPiggy.eat();
        Assert.assertEquals(1, missPiggy.getWeight());
    }

    @Test
    public void overrideTest() {
        Animal pig = new Porker(2);
        pig.eat();
        Assert.assertEquals(2, pig.getWeight());
    }

}
