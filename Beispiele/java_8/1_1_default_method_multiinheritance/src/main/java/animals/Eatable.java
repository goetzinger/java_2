package animals;

/**
 * Created by goetzingert on 12.03.16.
 */
public interface Eatable {

    default void eat()
    {
        System.out.println("I eat");
    }
}
