package animals;


import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

/**
 * Created by goetzingert on 11.03.16.
 */
public interface Animal {

    int getWeight();

    void setWeight(int weight);

    default void eat() {
        setWeight(getWeight() + 1);
    }


    static Collection<Animal> filterAllHeavyWeight(Collection<Animal> toFilter, int bound) {
        List<Animal> toReturn = new ArrayList<Animal>();
        for (Animal animal : toFilter) {
            if (animal.getWeight() > bound)
                toReturn.add(animal);
        }
        return toReturn;
    }


}
