package animals;

/**
 * Created by goetzingert on 11.03.16.
 */
public class Porker extends Schwein{

    private final int quality;

    public Porker(int quality) {
        this.quality = quality;
    }


    @Override
    public void eat() {
        for (int i = 0; i < quality; i++) {
            super.eat();
        }
    }
}
