package functions;/*
 * Created on 20.11.2006 To change the template for this generated file go to Window&gt;Preferences&gt;Java&gt;Code Generation&gt;Code and
 * Comments
 */

/**
 * Generische Schnittstelle f&uuml;r alle zweidimensionalen Funktionen.
 * 
 * @author goetzingert
 */
public interface  BinaryFunction<Return, Param1, Param2>
{
  Return execute(Param1 p1, Param2 p2);

}
