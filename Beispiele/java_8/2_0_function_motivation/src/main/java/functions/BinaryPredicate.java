package functions;/*
 * Created on 17.11.2006 To change the template for this generated file go to Window&gt;Preferences&gt;Java&gt;Code Generation&gt;Code and
 * Comments
 */

/**
 * Generische Schnittstelle f&uuml;r alle zweidimensionalen Pr&auml;dikate. Ein Pr&auml;dikat ist eine spezielle Funktion die angewendet auf
 * einen Eingabewert einen boolschen Wert zur&uuml;ckgibt.
 * 
 * @author goetzingert
 */
public interface BinaryPredicate<Param1, Param2>
{

  /**
   * Pr&auml;dikatsfunktion, die den Eingabeparametern einen boolschen Wert zuweisen.
   * 
   * @param p1
   * @param p2
   * @return
   */
  boolean execute(Param1 p1, Param2 p2);

}
