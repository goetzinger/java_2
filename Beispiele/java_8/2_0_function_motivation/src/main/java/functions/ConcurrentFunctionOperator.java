package functions; /**
 * 
 */

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.LinkedList;
import java.util.List;
import java.util.SortedSet;
import java.util.TreeSet;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.LinkedBlockingQueue;
import java.util.logging.Level;
import java.util.logging.Logger;

import functions.BinaryFunction;
import functions.BinaryPredicate;


/**
 * Implementierung von {@link IFunctionOperator} die die einzelnen Aufrufe von {@link UnaryFunction#execute(Object)}, {@link BinaryPredicate},
 * etc. angewendet auf eine Liste von Objekten nebenl�ufig ausf�hrt. Hierzu nutzt er den WorkerPool. Als Vorbedingung f�r die asynchrone
 * Ausf�hrung der Schleifen-Bodies gilt die freiheit von Seiteneffekten. Bedeutet: Nur wenn sich die einzelnen Schleifenaufrufe sich nicht
 * gegenseitig beeintr�chtigen, k�nnen sie nebenl�ufig ausgef�hrt werden.<br/>
 * Hierzu wurde die Annotation {@link SideEffectFree} eingef�gt. Nur wenn die Funktion bzw. das Pr�dikat damit annotiert wurde, wird sie f�r die
 * nebenl�ufige Ausf�hrung betrachtet.
 * 
 * @author goetzingert
 * @since 12.3
 * @see SideEffectFree
 */
public class ConcurrentFunctionOperator
    implements IFunctionOperator
{

  /**
   * Delegate an den synchrone Abarbeitung weitergeleitet wird.
   */
  private IFunctionOperator delegate;

  /**
   * Pool, der die Asynchrone Ausf�hrung der Functionen, Pr�dikate �bernimmt.
   */
  private ExecutorService       workerPool;

  /**
   * @see functions.IFunctionOperator#every(functions.BinaryPredicate,
   *      Object, Collection)
   */
  @Override
  public <Param1, Param2> boolean every(final BinaryPredicate<Param1, Param2> pred, final Param2 param2, Collection<Param1> coll)
  {
    if (isSideEffectFree(pred.getClass()))
    {
      LinkedBlockingQueue<AsynchresultToken<Boolean>> resultList = executePredicateConcurrent(pred, param2, new ArrayList<Param1>(coll));
      return catchResultsOfPredicates4Every(coll.size(), resultList);
    }
    else
      return delegate.every(pred, param2, coll);
  }

  private <Param1> boolean catchResultsOfPredicates4Every(int nrOfPrdicateCalls,
      final LinkedBlockingQueue<AsynchresultToken<Boolean>> resultsOfPredicates)
  {
    for (int i = 0; i < nrOfPrdicateCalls; i++)
    {
      try
      {
        if (!resultsOfPredicates.take().result)
          return false;
      }
      catch (InterruptedException e)
      {
        Logger.getLogger(this.getClass().getName()).log(Level.SEVERE,"Failure",e);
      }
    }
    return true;
  }

  private <Param2, Param1> LinkedBlockingQueue<AsynchresultToken<Boolean>> executePredicateConcurrent(
      final BinaryPredicate<Param1, Param2> pred, final Param2 param2, final List<Param1> list)
  {
    final LinkedBlockingQueue<AsynchresultToken<Boolean>> resultList = new LinkedBlockingQueue<AsynchresultToken<Boolean>>(list.size());
    for (final Param1 param1 : list)
    {
      workerPool.submit(new Runnable() {
        @Override
        public void run() {
          boolean execute = false;
          try {
            execute = pred.execute(param1, param2);
          } finally {
            catchResult(resultList, new AsynchresultToken<Boolean>(execute, list.indexOf(param1)));
          }
        }
      });
    }
    return resultList;
  }

  private <Param1,Return> void catchResult(LinkedBlockingQueue<AsynchresultToken<Return>> resultList, AsynchresultToken<Return> e){
    try {
      resultList.put(e);
    } catch (InterruptedException e1) {
      Logger.getLogger(this.getClass().getName()).log(Level.SEVERE,"failure",e);
    }

  }

  private <Param2, Param1> LinkedBlockingQueue<AsynchresultToken<Boolean>> executePredicateConcurrent(final UnaryPredicate<Param1> pred,
      final List<Param1> list)
  {
    final LinkedBlockingQueue<AsynchresultToken<Boolean>> resultsOfPredicates = new LinkedBlockingQueue<AsynchresultToken<Boolean>>(list.size());
    for (final Param1 param1 : list)
    {
      workerPool.submit(new Runnable() {
        @Override
        public void run() {
          boolean execute = false;
          try {
            execute = pred.execute(param1);
          } finally {
            catchResult(resultsOfPredicates, new AsynchresultToken<Boolean>(execute, list.indexOf(param1)));
          }
        }
      });
    }
    return resultsOfPredicates;
  }

  /**
   * @see functions.IFunctionOperator#every(functions.UnaryPredicate,
   *      Collection)
   */
  @Override
  public <Param1> boolean every(UnaryPredicate<Param1> pred, Collection<Param1> coll)
  {
    if (isSideEffectFree(pred.getClass()))
    {
      final LinkedBlockingQueue<AsynchresultToken<Boolean>> resultList = executePredicateConcurrent(pred, new ArrayList<Param1>(coll));
      return catchResultsOfPredicates4Every(coll.size(), resultList);
    }
    else
      return delegate.every(pred, coll);
  }

  /**
   * @see functions.IFunctionOperator#filter(functions.UnaryPredicate,
   *      Collection)
   */
  @Override
  public <Param1> List<Param1> filter(UnaryPredicate<Param1> pred, Collection<Param1> coll)
  {
    if (isSideEffectFree(pred.getClass()))
    {
      List<Param1> coll2List = new ArrayList<Param1>(coll);
      final LinkedBlockingQueue<AsynchresultToken<Boolean>> resultList = executePredicateConcurrent(pred, coll2List);
      return catchResultsOfPredicates4Filter(coll2List, resultList);
    }
    return delegate.filter(pred, coll);
  }

  private <Param1> List<Param1> catchResultsOfPredicates4Filter(List<Param1> input,
      LinkedBlockingQueue<AsynchresultToken<Boolean>> resultsOfPredicates)
  {
    List<Param1> resultList = new LinkedList<Param1>();
    for (int i = 0; i < input.size(); i++)
    {
      try
      {
        AsynchresultToken<Boolean> threadResult = resultsOfPredicates.take();
        if (threadResult.result)
          resultList.add(input.get(threadResult.indexOfTask));
      }
      catch (InterruptedException ie)
      {
        Logger.getLogger(this.getClass().getName()).log(Level.SEVERE, "failure", ie);
      }
    }
    return resultList;
  }

  /**
   * @see functions.IFunctionOperator#filter(functions.BinaryPredicate,
   *      Collection, Object)
   */
  @Override
  public <Param1, Param2> List<Param1> filter(BinaryPredicate<Param1, Param2> pred, Collection<Param1> coll, Param2 constant)
  {
    if (isSideEffectFree(pred.getClass()))
    {
      List<Param1> coll2List = new ArrayList<Param1>(coll);
      final LinkedBlockingQueue<AsynchresultToken<Boolean>> resultList = executePredicateConcurrent(pred, constant, coll2List);
      return catchResultsOfPredicates4Filter(coll2List, resultList);
    }
    return delegate.filter(pred, coll, constant);
  }

  /**
   * @see functions.IFunctionOperator#filter(functions.BinaryPredicate,
   *      Param1[], Object)
   */
  @Override
  public <Param1, Param2> List<Param1> filter(BinaryPredicate<Param1, Param2> pred, Param1[] coll, Param2 constant)
  {
    if (isSideEffectFree(pred.getClass()))
    {
      List<Param1> coll2List = Arrays.asList(coll);
      final LinkedBlockingQueue<AsynchresultToken<Boolean>> resultList = executePredicateConcurrent(pred, constant, coll2List);
      return catchResultsOfPredicates4Filter(coll2List, resultList);
    }
    return delegate.filter(pred, coll, constant);
  }

  /**
   * @see functions.IFunctionOperator#forEach(functions.UnaryFunction,
   *      Collection)
   */
  @Override
  public <Result, Param1> void forEach(UnaryFunction<Result, Param1> function, Collection<Param1> coll)
  {
    if (isSideEffectFree(function.getClass()))
    {
      LinkedBlockingQueue queue = this.executeFunctionConcurrent(function, new ArrayList(coll));
      waitUntilAllResultsPresent(coll, queue);
    }
    delegate.forEach(function, coll);
  }

  private <Param1> void waitUntilAllResultsPresent(Collection<Param1> coll, LinkedBlockingQueue queue)
  {
    while (queue.size() != coll.size())
      Thread.yield();
  }

  private <Result, Param1> LinkedBlockingQueue<AsynchresultToken<Result>> executeFunctionConcurrent(
      final UnaryFunction<Result, Param1> function, final List<Param1> list)
  {
    final LinkedBlockingQueue<AsynchresultToken<Result>> resultsOfFunction = new LinkedBlockingQueue<AsynchresultToken<Result>>(list.size());
    for (final Param1 param1 : list)
    {
      workerPool.submit(new Runnable() {
        @Override
        public void run() {
          Result execute = null;
          try {
            execute = function.execute(param1);
          } finally {
            catchResult(resultsOfFunction, new AsynchresultToken<Result>(execute, list.indexOf(param1)));
          }
        }
      });
    }
    return resultsOfFunction;
  }

  private <Result, Param1, Param2> LinkedBlockingQueue<AsynchresultToken<Result>> executeFunctionConcurrent(
      final BinaryFunction<Result, Param1, Param2> function, final List<Param1> list, final Param2 constant)
  {
    final LinkedBlockingQueue<AsynchresultToken<Result>> resultsOfFunction = new LinkedBlockingQueue<AsynchresultToken<Result>>(list.size());
    for (final Param1 param1 : list)
    {
      workerPool.submit(new Runnable() {
        @Override
        public void run() {
          Result execute = null;
          try {
            execute = function.execute(param1, constant);
          } finally {
            catchResult(resultsOfFunction,new AsynchresultToken<Result>(execute, list.indexOf(param1)));
          }
        }
      });
    }
    return resultsOfFunction;
  }

  /**
   * @see functions.IFunctionOperator#forEach(functions.BinaryFunction,
   *      Collection, Object)
   */
  @Override
  public <Result, Param1, Param2> void forEach(BinaryFunction<Result, Param1, Param2> function, Collection<Param1> coll, Param2 param2)
  {
    if (isSideEffectFree(function.getClass()))
    {
      LinkedBlockingQueue queue = this.executeFunctionConcurrent(function, new ArrayList(coll), param2);
      waitUntilAllResultsPresent(coll, queue);
    }
    delegate.forEach(function, coll, param2);
  }

  /**
   * @see functions.IFunctionOperator#forEach(functions.BinaryFunction,
   *      Param1[], Object)
   */
  @Override
  public <Result, Param1, Param2> void forEach(BinaryFunction<Result, Param1, Param2> function, Param1[] coll, Param2 param2)
  {
    if (isSideEffectFree(function.getClass()))
    {
      List<Param1> list = Arrays.asList(coll);
      LinkedBlockingQueue queue = this.executeFunctionConcurrent(function, list, param2);
      waitUntilAllResultsPresent(list, queue);
    }
    delegate.forEach(function, coll, param2);
  }

  /**
   * @see functions.IFunctionOperator#map(functions.UnaryFunction,
   *      Collection)
   */
  @Override
  public <Result, Param1> List<Result> map(UnaryFunction<Result, Param1> function, Collection<? extends Param1> coll)
  {
    if (isSideEffectFree(function.getClass()))
    {
      LinkedBlockingQueue queue = this.executeFunctionConcurrent(function, new ArrayList(coll));
      return catchResultsOfFunction(queue, coll.size());
    }
    return delegate.map(function, coll);
  }

  private <Result, Param1> List<Result> catchResultsOfFunction(LinkedBlockingQueue<AsynchresultToken<Result>> results, int nrOfFunctionCalls)
  {
    List<Result> resultList = new ArrayList<Result>(nrOfFunctionCalls);
    SortedSet<AsynchresultToken<Result>> sortedSet = new TreeSet<AsynchresultToken<Result>>(new TokenComparator());
    for (int i = 0; i < nrOfFunctionCalls; i++)
    {
      try
      {
        sortedSet.add(results.take());
      }
      catch (InterruptedException e)
      {
        Logger.getLogger(this.getClass().getName()).log(Level.SEVERE, "failure", e);
      }
    }
    for (AsynchresultToken<Result> asynchresultToken : sortedSet)
    {
      resultList.add(asynchresultToken.result);
    }
    return resultList;
  }

  /**
   * @see functions.IFunctionOperator#map(functions.UnaryFunction,
   *      Param1[])
   */
  @Override
  public <Result, Param1> List<Result> map(UnaryFunction<Result, Param1> function, Param1[] array)
  {
    if (isSideEffectFree(function.getClass()))
    {
      LinkedBlockingQueue queue = this.executeFunctionConcurrent(function, Arrays.asList(array));
      return catchResultsOfFunction(queue, array.length);
    }
    return delegate.map(function, array);
  }

  /**
   * @see functions.IFunctionOperator#map(functions.BinaryFunction,
   *      Collection, Object)
   */
  @Override
  public <Result, Param1, Param2> List<Result> map(BinaryFunction<Result, Param1, Param2> function, Collection<Param1> coll, Param2 param2)
  {
    if (isSideEffectFree(function.getClass()))
    {
      LinkedBlockingQueue queue = this.executeFunctionConcurrent(function, coll instanceof List ? ((List) coll) : new ArrayList(coll), param2);
      return catchResultsOfFunction(queue, coll.size());
    }
    return delegate.map(function, coll, param2);
  }

  /**
   * @see functions.IFunctionOperator#reduce(functions.BinaryFunction,
   *      Collection, Object)
   */
  @Override
  public <Return, Param> Return reduce(BinaryFunction<Return, Param, Return> func, Collection<Param> coll, Return initialValue)
  {
    return delegate.reduce(func, coll, initialValue);
  }

  /**
   * @see functions.IFunctionOperator#some(functions.UnaryPredicate,
   *      Collection)
   */
  @Override
  public <Param1> boolean some(UnaryPredicate<Param1> pred, Collection<Param1> coll)
  {
    if (isSideEffectFree(pred.getClass()))
    {
      LinkedBlockingQueue concurrentResults = this.executePredicateConcurrent(pred, coll instanceof List ? ((List) coll) : new ArrayList(coll));
      return this.catchResultsOfPredicates4Some(coll.size(), concurrentResults);
    }
    return delegate.some(pred, coll);
  }

  private <Param1> boolean catchResultsOfPredicates4Some(int nrOfPrdicateCalls,
      final LinkedBlockingQueue<AsynchresultToken<Boolean>> resultsOfPredicates)
  {
    for (int i = 0; i < nrOfPrdicateCalls; i++)
    {
      try
      {
        if (resultsOfPredicates.take().result)
          return true;
      }
      catch (InterruptedException e)
      {
        Logger.getLogger(this.getClass().getName()).log(Level.SEVERE, "Failure", e);
      }
    }
    return false;
  }

  /**
   * @see functions.IFunctionOperator#some(functions.UnaryPredicate,
   *      Param1[])
   */
  @Override
  public <Param1> boolean some(UnaryPredicate<Param1> pred, Param1[] array)
  {
    if (isSideEffectFree(pred.getClass()))
    {
      LinkedBlockingQueue concurrentResults = this.executePredicateConcurrent(pred, Arrays.asList(array));
      return this.catchResultsOfPredicates4Some(array.length, concurrentResults);
    }
    return delegate.some(pred, array);
  }

  /**
   * @see functions.IFunctionOperator#some(functions.BinaryPredicate,
   *      Collection, Object)
   */
  @Override
  public <Param1, Param2> boolean some(BinaryPredicate<Param1, Param2> pred, Collection<Param1> coll, Param2 param2)
  {
    if (isSideEffectFree(pred.getClass()))
    {
      LinkedBlockingQueue concurrentResults = this.executePredicateConcurrent(pred, param2, coll instanceof List ? ((List) coll)
          : new ArrayList(coll));
      return this.catchResultsOfPredicates4Some(coll.size(), concurrentResults);
    }
    return delegate.some(pred, coll, param2);
  }

  /**
   * @see functions.IFunctionOperator#some(functions.BinaryPredicate,
   *      Param1[], Object)
   */
  @Override
  public <Param1, Param2> boolean some(BinaryPredicate<Param1, Param2> pred, Param1[] array, Param2 param2)
  {
    if (isSideEffectFree(pred.getClass()))
    {
      LinkedBlockingQueue concurrentResults = this.executePredicateConcurrent(pred, param2, Arrays.asList(array));
      return this.catchResultsOfPredicates4Some(array.length, concurrentResults);
    }
    return delegate.some(pred, array, param2);
  }

  /**
   *
   */
  public void setWorkerPool(ExecutorService workerPool)
  {
    this.workerPool = workerPool;
  }

  /**
   * @param delegate the delegate to set
   */
  public void setDelegate(IFunctionOperator delegate)
  {
    this.delegate = delegate;
  }

  private boolean isSideEffectFree(Class c)
  {
    return c.isAnnotationPresent(SideEffectFree.class);
  }

  /**
   * Klasse zur Ergebnissweitergabe zwischen Threads benutzt wird.
   * 
   * @author goetzingert
   * @param <T> Typ des Ergebnis eines Threadlaufs.
   * @since 12.3
   */
  class AsynchresultToken<T>
  {

    /**
     * Resultat des asynchronen Aufrufs.
     */
    T   result;
    /**
     * Index des Tasks im �bergeordneten Vorgang.
     */
    int indexOfTask;

    /**
     * 
     */
    public AsynchresultToken(T result, int indexOfTask)
    {
      this.result = result;
      this.indexOfTask = indexOfTask;
    }

  }

}
