package functions; /**
 * 
 */

import functions.BinaryFunction;
import functions.BinaryPredicate;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import java.util.function.BiPredicate;

/**
 * @see IFunctionOperator
 * @author goetzingert
 * @since 12.3
 */
public class FunctionOperator
    implements IFunctionOperator
{

  /**
   * @see functions.IFunctionOperator#every(functions.BinaryPredicate,
   *      Object, Collection)
   */
  @Override
  public <Param1, Param2> boolean every(BinaryPredicate<Param1, Param2> pred, Param2 param2, Collection<Param1> list)
  {


    boolean retVal = true;
    if (list != null)
    {
      synchronized (list)
      {
        for (Param1 param1 : list)
        {
          retVal = retVal && pred.execute(param1, param2);
          if (!retVal)
            break;
        }
      }
    }
    return retVal;
  }

  /**
   * @see functions.IFunctionOperator#every(functions.UnaryPredicate,
   *      Collection)
   */
  @Override
  public <Param1> boolean every(UnaryPredicate<Param1> pred, Collection<Param1> list)
  {
    boolean retVal = true;
    synchronized (list)
    {
      for (Param1 param1 : list)
      {
        retVal = retVal && pred.execute(param1);
        if (!retVal)
          break;
      }
    }
    return retVal;
  }

  /**
   * @see functions.IFunctionOperator#filter(functions.UnaryPredicate,
   *      Collection)
   */
  @Override
  public <Param1> List<Param1> filter(UnaryPredicate<Param1> pred, Collection<Param1> coll)
  {
    List<Param1> toReturn = new ArrayList<Param1>();
    synchronized (coll)
    {
      for (Param1 current : coll)
      {
        if (pred.execute(current))
          toReturn.add(current);
      }
    }
    return toReturn;
  }

  /**
   * @see functions.IFunctionOperator#filter(functions.BinaryPredicate,
   *      Collection, Object)
   */
  @Override
  public <Param1, Param2> List<Param1> filter(BinaryPredicate<Param1, Param2> pred, Collection<Param1> coll, Param2 constant)
  {
    List<Param1> toReturn = new ArrayList<Param1>();
    synchronized (coll)
    {
      for (Param1 current : coll)
      {
        if (pred.execute(current, constant))
          toReturn.add(current);
      }
    }
    return toReturn;
  }

  /**
   * @see functions.IFunctionOperator#filter(functions.BinaryPredicate,
   *      Param1[], Object)
   */
  @Override
  public <Param1, Param2> List<Param1> filter(BinaryPredicate<Param1, Param2> pred, Param1[] coll, Param2 constant)
  {
    List<Param1> toReturn = new ArrayList<Param1>();
    synchronized (coll)
    {
      for (Param1 current : coll)
      {
        if (pred.execute(current, constant))
          toReturn.add(current);
      }
    }
    return toReturn;
  }

  /**
   * @see functions.IFunctionOperator#forEach(functions.UnaryFunction,
   *      Collection)
   */
  @Override
  public <Result, Param1> void forEach(UnaryFunction<Result, Param1> function, Collection<Param1> coll)
  {
    synchronized (coll)
    {
      for (Param1 current : coll)
      {
        function.execute(current);
      }
    }
  }

  /**
   * @see functions.IFunctionOperator#forEach(functions.BinaryFunction,
   *      Collection, Object)
   */
  @Override
  public <Result, Param1, Param2> void forEach(BinaryFunction<Result, Param1, Param2> function, Collection<Param1> coll, Param2 param2)
  {
    synchronized (coll)
    {
      for (Param1 current : coll)
      {
        function.execute(current, param2);
      }
    }

  }

  /**
   * @see functions.IFunctionOperator#forEach(functions.BinaryFunction,
   *      Param1[], Object)
   */
  @Override
  public <Result, Param1, Param2> void forEach(BinaryFunction<Result, Param1, Param2> function, Param1[] coll, Param2 param2)
  {
    synchronized (coll)
    {
      for (Param1 current : coll)
      {
        function.execute(current, param2);
      }
    }
  }

  /**
   * @see functions.IFunctionOperator#map(functions.UnaryFunction,
   *      Collection)
   */
  @Override
  public <Result, Param1> List<Result> map(UnaryFunction<Result, Param1> function, Collection<? extends Param1> coll)
  {
    List<Result> toReturn = new ArrayList<Result>(coll.size());
    synchronized (coll)
    {
      for (Param1 current : coll)
      {
        toReturn.add(function.execute(current));
      }
    }
    return toReturn;
  }

  /**
   * @see functions.IFunctionOperator#map(functions.UnaryFunction,
   *      Param1[])
   */
  @Override
  public <Result, Param1> List<Result> map(UnaryFunction<Result, Param1> function, Param1[] array)
  {
    List<Result> toReturn = new ArrayList<Result>(array.length);
    synchronized (array)
    {
      for (Param1 current : array)
      {
        toReturn.add(function.execute(current));
      }
    }
    return toReturn;
  }

  /**
   * @see functions.IFunctionOperator#map(functions.BinaryFunction,
   *      Collection, Object)
   */
  @Override
  public <Result, Param1, Param2> List<Result> map(BinaryFunction<Result, Param1, Param2> function, Collection<Param1> coll, Param2 param2)
  {
    List<Result> toReturn = new ArrayList<Result>(coll.size());
    synchronized (coll)
    {
      for (Param1 current : coll)
      {
        toReturn.add(function.execute(current, param2));
      }
    }
    return toReturn;
  }

  /**
   * @see functions.IFunctionOperator#reduce(functions.BinaryFunction,
   *      Collection, Object)
   */
  @Override
  public <Return, Param> Return reduce(BinaryFunction<Return, Param, Return> func, Collection<Param> coll, Return initialValue)
  {
    Return result = initialValue;
    synchronized (coll)
    {
      for (Param current : coll)
      {
        func.execute(current, result);
      }
    }
    return result;
  }

  /**
   * @see functions.IFunctionOperator#some(functions.UnaryPredicate,
   *      Collection)
   */
  @Override
  public <Param1> boolean some(UnaryPredicate<Param1> pred, Collection<Param1> coll)
  {
    boolean retVal = false;
    synchronized (coll)
    {
      for (Param1 param1 : coll)
      {
        retVal = retVal || pred.execute(param1);
        if (retVal)
          break;
      }
    }
    return retVal;
  }

  /**
   * @see functions.IFunctionOperator#some(functions.UnaryPredicate,
   *      Param1[])
   */
  @Override
  public <Param1> boolean some(UnaryPredicate<Param1> pred, Param1[] coll)
  {
    return false;
  }

  /**
   * @see functions.IFunctionOperator#some(functions.BinaryPredicate,
   *      Collection, Object)
   */
  @Override
  public <Param1, Param2> boolean some(BinaryPredicate<Param1, Param2> pred, Collection<Param1> coll, Param2 param2)
  {
    boolean retVal = false;
    synchronized (coll)
    {
      for (Param1 param1 : coll)
      {
        retVal = retVal || pred.execute(param1, param2);
        if (retVal)
          break;
      }
    }
    return retVal;
  }

  /**
   * @see functions.IFunctionOperator#some(functions.BinaryPredicate,
   *      Param1[], Object)
   */
  @Override
  public <Param1, Param2> boolean some(BinaryPredicate<Param1, Param2> pred, Param1[] coll, Param2 param2)
  {
    boolean retVal = false;
    synchronized (coll)
    {
      for (Param1 param1 : coll)
      {
        retVal = retVal || pred.execute(param1, param2);
        if (retVal)
          break;
      }
    }
    return retVal;
  }
}
