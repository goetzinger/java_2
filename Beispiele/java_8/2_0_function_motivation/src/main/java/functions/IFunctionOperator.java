package functions; /**
 * 
 */

import java.util.Collection;
import java.util.List;

/**
 * Pool zur Nutzung generischer Funktionen h&ouml;herer Ordnung.<br/>
 * Generische Funktionen h&ouml;herer Ordnung definieren wiederverwendbare abstrakte Abl&auml;ufe. Zum verwenden der generischen Funktionen muss
 * eine Implementierung des jeweiligen Pr&auml;dikat- bzw. Funktionsinterface geschrieben werden. <br/>
 * Beispiel: <b>every</b>
 * <p>
 * 
 * <pre>
 * public class IsNotNull
 *     implements functions.UnaryPredicate&lt;IBusinessObject&gt;
 * {
 *   public boolean execute(IBusinessObject o)
 *   {
 *     return o != null;
 *   }
 * }
 * </pre>
 * <p/>
 * Die schlie&szlig;liche Nutzung der obigen Implementierung zeigt folgendes Beispiel:
 * 
 * <pre>
 *      //Liste von Objekten initialisieren
 *      functions.IFunctionOperator operator = ...
 *      List l = ...
 *      boolean ergebnis = operator.every(new IsNotNull(), l);
 * </pre>
 * 
 * Den Operator erh�lt eine Klasse in dem Sie sich mit dem Interface {@link IFunctionOperatorAware} markiert. Funktioniert nat�rlich nur im
 * DI-Container (Spring).
 * 
 * @author goetzingert
 */
public interface IFunctionOperator
{

  /**
   * Methode, die f&uuml;r jedes Element aus der Collection list mithilfe des bin&auml;ren Pr&auml;dikates pred eine boolschen Ausdruck auswertet
   * und alle Ergebnisse mit einer Und-Verkn�pfung zu einem Ergebnis der Methode verkn&uuml;pft.
   * 
   * @param <Param1> Klasse des 1. Parameters des bin&auml;ren Pr&auml;dikates.
   * @param <Param2> Klasse des 2. Parameters des bin&auml;ren Pr&auml;dikates.
   * @param pred Bin&auml;res Pr&auml;dikat.
   * @param list Collection der Elemente auf die das bin&auml;re Pr&auml;dikat anzuwenden ist.
   * @return True, falls die Auswertung des Pr&auml;dikates angewendet auf jedes Element der Collection true ergibt.<br/>
   *         False, sonst.
   */
  <Param1, Param2> boolean every(BinaryPredicate<Param1, Param2> pred, Param2 param2, Collection<Param1> list);

  /**
   * Methode, l&auml;uft &uuml;ber die Collection und berechnet das Ergebnis der Funktion <code>func</code> der vorangegangen Durchlaufes und des
   * gerade betrachteten Elementes der Collection. <br/>
   * Zur Berechnung des Funktionsergebnisses beim ersten Durchlauf wird der Parameter <code>initialValue</code> genutzt. Hier k&ouml;nnte man
   * z.B. eine Summenfunktion anwenden, die, die Summe der Elemente in der Collection berechnet. Der Startparameter w&auml;re hier z.B. die
   * &quot;0&quot;.
   * 
   * @param <Param>
   * @param func Bin&auml;re Funktion die zum &quot;Falten&quot; der Collection genutzt wird.
   * @param coll Elemente auf die, die Funktion angewendet werden soll.
   * @param initialValue Startparameter f&uuml;r den Itertationsprozess
   * @return
   */
  <Return, Param> Return reduce(BinaryFunction<Return, Param, Return> func, Collection<Param> coll, Return initialValue);

  /**
   * Methode, die f&uuml;r jedes Element aus der Collection list mithilfe des un&auml;ren Pr&auml;dikates pred eine boolschen Ausdruck auswertet
   * und alle Ergebnisse mit einer Und-Verkn�pfung zu einem Ergebnis der Methode verkn&uuml;pft.
   * 
   * @param <Param1> Klasse des 1. Parameters des bin&auml;ren Pr&auml;dikates.
   * @param <Param2> Klasse des 2. Parameters des bin&auml;ren Pr&auml;dikates.
   * @param pred Un&auml;res Pr&auml;dikat.
   * @param list Collection der Elemente auf die das un&auml;re Pr&auml;dikat anzuwenden ist.
   * @return True, falls die Auswertung des Pr&auml;dikates angewendet auf jedes Element der Collection true ergibt.<br/>
   *         False, sonst.
   */
  <Param1> boolean every(UnaryPredicate<Param1> pred, Collection<Param1> list);

  /**
   * Methode, die f&uuml;r jedes Element aus der Collection coll mithilfe des un&auml;ren Pr&auml;dikates pred eine boolschen Ausdruck auswertet
   * und alle Ergebnisse mit einer Oder-Verkn�pfung zu einem Ergebnis der Methode verkn&uuml;pft.
   * 
   * @param <Param1> Klasse des 1. Parameters des bin&auml;ren Pr&auml;dikates.
   * @param pred Un&auml;res Pr&auml;dikat.
   * @param coll Collection der Elemente auf die das un&auml;re Pr&auml;dikat anzuwenden ist.
   * @return True, falls die Anwendung des Pr&auml;dikates auf ein Element der collection true ergibt.<br/>
   *         False, sonst.
   */
  <Param1> boolean some(UnaryPredicate<Param1> pred, Collection<Param1> coll);

  /**
   * @see UpperRangFunctions#some(UnaryPredicate, Collection)
   */
  <Param1> boolean some(UnaryPredicate<Param1> pred, Param1[] coll);

  /**
   * Methode, die f&uuml;r jedes Element aus der Collection coll mithilfe des bin&auml;ren Pr&auml;dikates pred eine boolschen Ausdruck auswertet
   * und alle Ergebnisse mit einer Oder-Verkn�pfung zu einem Ergebnis der Methode verkn&uuml;pft.
   * 
   * @param <Param1> Klasse des 1. Parameters des bin&auml;ren Pr&auml;dikates.
   * @param <Param2> Klasse des 2. Parameters des bin&auml;ren Pr&auml;dikates.
   * @param pred Un&auml;res Pr&auml;dikat.
   * @param coll Collection der Elemente auf die das un&auml;re Pr&auml;dikat anzuwenden ist.
   * @param param2 Element das bei jedem Element auf das Pr&auml;dikat angewendet wird.
   * @return True, falls die Anwendung des Pr&auml;dikates auf ein Element der collection true ergibt.<br/>
   *         False, sonst.
   */
  <Param1, Param2> boolean some(BinaryPredicate<Param1, Param2> pred, Collection<Param1> coll, Param2 param2);

  /**
   * @see #some(BinaryPredicate, Collection, Object)
   */
  <Param1, Param2> boolean some(BinaryPredicate<Param1, Param2> pred, Param1[] coll, Param2 param2);

  /**
   * Methode, die, die Elemente aus der collection zur�ckgibt, die angewendet auf das Pr&auml;dikat true ergibt.
   * 
   * @param <Param1> Klasse des Ergebnisses des un&auml;ren Pr&auml;dikates.
   * @param pred Un&auml;res Pr&auml;dikat zum Filtern der Elemente.
   * @param coll Collection aus der die Elemente gefiltert werden sollen.
   * @return Liste mit Elementen aus der collection, die angewendet auf das Pr&auml;dikat true ergibt.
   */
  <Param1> List<Param1> filter(UnaryPredicate<Param1> pred, Collection<Param1> coll);

  /**
   * Methode, die, die Elemente aus der collection zur�ckgibt, die angewendet auf das Pr&auml;dikat true ergibt.
   * 
   * @param <Param1> Klasse des 1. Eingabeparameters des un&auml;ren Pr&auml;dikates.
   * @param <Param2> Klasse des konstanten 2. Eingabeparameters des un&auml;ren Pr&auml;dikates.
   * @param pred Bin&auml;res Pr&auml;dikat zum Filtern der Elemente.
   * @param coll Collection aus der die Elemente gefiltert werden sollen.
   * @param constant Konstante die zur Entscheidung im Pr�dikat rangezogen werden muss.
   * @return Liste mit Elementen aus der collection, die angewendet auf das Pr&auml;dikat true ergibt.
   */
  <Param1, Param2> List<Param1> filter(BinaryPredicate<Param1, Param2> pred, Collection<Param1> coll, Param2 constant);

  /**
   * @see #filter(BinaryPredicate, Collection, Object)
   */
  <Param1, Param2> List<Param1> filter(BinaryPredicate<Param1, Param2> pred, Param1[] coll, Param2 constant);

  /**
   * Methode, die, die un&auml;re Funktion <code>function</code> auf alle Elemente der Collection anwendet. Das jeweilige Ergebnis des
   * Funktionsaufrufes wird in die R&uuml;ckgabeliste geschrieben. D.h. an Stelle i der Ergebnisliste steht der Funktionswert von function
   * angewendet auf den Parameter an Stelle i der Collection.
   * 
   * @param <Result> Klasse des Ergebnisses der un&auml;ren Funktion.
   * @param <Param1> Klasse des 1. Parameters des un&auml;ren Funktion.
   * @param function Un&auml;re Funktion die auf jedes Element der Collection angewendet werden soll.
   * @param coll Collection mit Parametern.
   * @return Eine Liste in der an Stelle i der Funktionswert von <code>function</code> angewendet auf das i-te Element der Collection steht.
   */
  <Result, Param1> List<Result> map(UnaryFunction<Result, Param1> function, Collection<? extends Param1> coll);

  /**
   * Gleiche Methode wie {@link #map(UnaryFunction, Collection)} nur mittels Array.
   * 
   * @param addLayerForViewFunction
   * @param viewsAdded
   * @see #map(UnaryFunction, Collection)
   */
  <Result, Param1> List<Result> map(UnaryFunction<Result, Param1> function, Param1[] array);

  /**
   * Methode, die, die bin&auml;re Funktion <code>function</code> auf alle Elemente der Collection anwendet. Das jeweilige Ergebnis des
   * Funktionsaufrufes wird in die R&uuml;ckgabeliste geschrieben. D.h. an Stelle i der Ergebnisliste steht der Funktionswert von function
   * angewendet auf den Parameter an Stelle i der Collection.
   * 
   * @param <Result> Klasse des Ergebnisses der bin&auml;ren Funktion.
   * @param <Param1> Klasse des 1. Parameters des bin&auml;ren Funktion.
   * @param function Bin&auml;re Funktion die auf jedes Element der Collection angewendet werden soll.
   * @param coll Collection mit Parametern.
   * @return Eine Liste in der an Stelle i der Funktionswert von <code>function</code> angewendet auf das i-te Element der Collection steht.
   */
  <Result, Param1, Param2> List<Result> map(BinaryFunction<Result, Param1, Param2> function, Collection<Param1> coll, Param2 param2);

  /**
   * Methode, die, die un&auml;re Funktion <code>function</code> auf alle Elemente der Collection anwendet.
   * 
   * @param <Result> Klasse des Ergebnisses der un&auml;ren Funktion.
   * @param <Param1> Klasse des 1. Parameters des un&auml;ren Funktion.
   * @param function Un&auml;re Funktion die auf jedes Element der Collection angewendet werden soll.
   * @param coll Collection mit Parametern.
   */
  <Result, Param1> void forEach(UnaryFunction<Result, Param1> function, Collection<Param1> coll);

  /**
   * Methode, die, die un&auml;re Funktion <code>function</code> auf alle Elemente der Collection anwendet.
   * 
   * @param <Result> Klasse des Ergebnisses der un&auml;ren Funktion.
   * @param <Param1> Klasse des 1. Parameters des un&auml;ren Funktion.
   * @param function Un&auml;re Funktion die auf jedes Element der Collection angewendet werden soll.
   * @param coll Collection mit Parametern.
   */
  <Result, Param1, Param2> void forEach(BinaryFunction<Result, Param1, Param2> function, Collection<Param1> coll, Param2 param2);

  /**
   * Methode, die, die un&auml;re Funktion <code>function</code> auf alle Elemente der Collection anwendet.
   * 
   * @param <Result> Klasse des Ergebnisses der un&auml;ren Funktion.
   * @param <Param1> Klasse des 1. Parameters des un&auml;ren Funktion.
   * @param function Un&auml;re Funktion die auf jedes Element der Collection angewendet werden soll.
   * @param coll Array mit Parametern.
   */
  <Result, Param1, Param2> void forEach(BinaryFunction<Result, Param1, Param2> function, Param1[] coll, Param2 param2);

}
