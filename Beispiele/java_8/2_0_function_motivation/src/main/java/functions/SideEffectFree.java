package functions; /**
 * 
 */

import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

/**
 * Annotation die angibt, dass eine Klasse und ihre Methoden frei von
 * Seiteneffekten ist. Somit sind die Methoden nebenl�ufig aufrufbar. Orgin�rer
 * Anwendungspunkt sind die Implementierungen von {@link UnaryFunction} bzw
 * {@link BinaryFunction} sowie {@link UpperRangFunctions}. Ist eine
 * {@link UnaryFunction} mit dieser Annotation versehen, k�nnte jede einzelne
 * Iteration z.B. von
 * {@link UpperRangFunctions#forEach(UnaryFunction, java.util.Collection)}
 * nebenl�ufig ausgef�hrt werden.
 * 
 * @author goetzingert
 */
@Target(ElementType.TYPE)
@Retention(RetentionPolicy.RUNTIME)
public @interface SideEffectFree
{

}
