package functions; /**
 *
 */


import java.util.Comparator;

/**
 * Comparator der zwei {@link AsynchresultToken} mithilfe des
 * {@link AsynchresultToken#indexOfTask} vergleicht.
 *
 * @author goetzingert
 * @since 12.3
 */
public class TokenComparator<Result>
        implements Comparator<ConcurrentFunctionOperator.AsynchresultToken<Result>> {

    /**
     * @see Comparator#compare(Object, Object)
     */
    @Override
    public int compare(ConcurrentFunctionOperator.AsynchresultToken<Result> o1, ConcurrentFunctionOperator.AsynchresultToken<Result> o2) {
        return o1.indexOfTask - o2.indexOfTask;
    }

}
