package functions;/*
 * Created on 20.11.2006 To change the template for this generated file go to Window&gt;Preferences&gt;Java&gt;Code Generation&gt;Code and
 * Comments
 */

/**
 * Generische Schnittstelle f&uuml;r alle einstellige Funktionen.
 * 
 * @author goetzingert
 */
public interface UnaryFunction<Result, Param1>
{
  Result execute(Param1 p1);
}
