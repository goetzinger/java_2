package functions;/*
 * Created on 17.11.2006 To change the template for this generated file go to Window&gt;Preferences&gt;Java&gt;Code Generation&gt;Code and
 * Comments
 */

/**
 * Generische Schnittstelle f&uuml;r alle eindimensionalen Pr&auml;dikate. Ein Pr&auml;dikat ist eine spezielle Funktion die angewendet auf einen
 * Eingabewert einen boolschen Wert zur&uuml;ckgibt.
 * 
 * @author goetzingert
 */
public interface UnaryPredicate<Param>
{

  /**
   * Pr&auml;dikatsfunktion, die dem Eingabeparameter einen boolschen Wert zuweisen.
   * 
   * @param param
   * @return
   */
  boolean execute(Param param);

}
