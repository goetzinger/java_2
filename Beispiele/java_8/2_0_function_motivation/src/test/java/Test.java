import functions.UnaryFunction;
import functions.UpperRangFunctions;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

/**
 * Created by goetzingert on 04.04.16.
 */
public class Test {

    public void howToUse(){
        List<String> stringList =  Arrays.asList("Horst","Paul","Tim");

        final List<String> firstCharacterOfStringList = UpperRangFunctions.map(new UnaryFunction<String, String>() {

            @Override
            public String execute(String p1) {
                return p1.substring(0, 1);
            }
        }, stringList);


    }
}
