/**
 * Created by goetzingert on 04.04.16.
 */
public class Schwein {

    private String name;

    private int gewicht;

    public Schwein(String name, int gewicht) {
        this.name = name;
        this.gewicht = gewicht;
    }

    public void fressen() {
        gewicht++;
    }

    public boolean is(SchweinPredicate predicate) {
        return predicate.check(name,gewicht);
    }


}
