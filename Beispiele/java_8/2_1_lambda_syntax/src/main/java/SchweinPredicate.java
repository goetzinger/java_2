/**
 * Created by goetzingert on 04.04.16.
 */
@FunctionalInterface
public interface SchweinPredicate {

    public boolean check(String name, int gewicht);
}
