package animals;

import java.util.UUID;

/**

/**
 * Created by goetzingert on 11.03.16.
 */
public class Pig implements Animal, Comparable<Pig> {

    private UUID id = UUID.randomUUID();

    private int weight;

    private String gender;

    public Pig() {
    }

    public Pig(int weight, String gender) {
        this.weight = weight;
        this.gender = gender;
    }

    @Override
    public int getWeight() {
        return weight;
    }

    @Override
    public void setWeight(int weight) {
        this.weight = weight;
    }

    public String getGender() {
        return gender;
    }

    public UUID getId() {
        return id;
    }

    @Override
    public int compareTo(Pig o) {
        if(o.getId().equals(getId()))
            return 0;
        final int weightDif = getWeight() - o.getWeight();
        if(weightDif != 0)
            return weightDif;
        final int genderCompare = getGender().compareTo(o.getGender());
        if(genderCompare != 0)
            return genderCompare;
        return getId().compareTo(o.getId());
    }

    @Override
    public String toString() {
        return "Pig{"+
                "weight=" + weight +
                ", gender='" + gender + '\'' +
                '}';
    }
}
