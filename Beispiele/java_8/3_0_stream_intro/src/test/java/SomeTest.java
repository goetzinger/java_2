import animals.Pig;
import org.junit.Test;

import java.util.*;

/**
 * Created by goetzingert on 18.03.16.
 */

public class SomeTest {

    private List<Pig> animals = new ArrayList<>();
    @Test
    public void showJava7Iteration(){
        List<Pig> boars = new ArrayList<>();
        //WHERE
        for (Pig animal : animals) {
            if("boar".equals(animal.getGender())){
                boars.add(animal);
            }
        }
        //ORDER
        Collections.sort(boars, new Comparator<Pig>() {
            @Override
            public int compare(Pig o1, Pig o2) {
                return o2.getWeight() - o1.getWeight();
            }
        });
        //SELECT ID
        List<UUID> boardIdsDesc = new ArrayList<>(boars.size());
        for (Pig boar : boars) {
            boardIdsDesc.add(boar.getId());
        }

    }
}
