import animals.Pig;
import org.junit.Test;

import java.util.*;
import java.util.stream.Collectors;
import java.util.stream.IntStream;
import java.util.stream.Stream;

/**
 * Created by goetzingert on 18.03.16.
 */
public class StreamSyntax {


    @Test
    public void creatingStreams() {
        Collection<String> strings = Arrays.asList("a", "b", "c");
        final Stream<String> stream = strings.stream();
        stream.forEach(System.out::println);


        final IntStream zeroTo10 = IntStream.range(0, 10);
        zeroTo10.forEach(System.out::println);

        int[] predefinedArray = new int[]{1, 2, 3};
        final IntStream stream1 = Arrays.stream(predefinedArray);

        Random random = new Random();
        final IntStream streamOfRandomNumber = random.ints();

    }


    @Test
    public void createAnimals() {
        List<Pig> animalList = new ArrayList<>();
        IntStream.range(0, 10).forEach(i -> animalList.add(new Pig(i * 10, randomGender(i))));
    }

    @Test
    public void createAnimalsWithMap() {
        final Pig[] pigs = IntStream.range(0, 10).mapToObj(i -> new Pig(i * 10, randomGender(i))).toArray((Pig[]::new));


        Arrays.asList(10, 20, 30, 40).stream().map(i -> new Pig(i, randomGender(i))).toArray(Pig[]::new);
    }

    @Test
    public void createAnimalsWithMapAndFilter() {
        final Pig[] pigs = IntStream.range(0, 10).mapToObj(i -> new Pig(i * 10, randomGender(i))).toArray((Pig[]::new));


        Arrays.asList(pigs).stream().filter(pig -> "boar".equalsIgnoreCase(pig.getGender())).forEach(pig -> System.out.println("foreach " + pig.getWeight()));
    }


    @Test
    public void createAnimalsWithMapAndToId() {
        final Pig[] pigs = IntStream.range(0, 10).mapToObj(i -> new Pig(i * 10, randomGender(i))).toArray((Pig[]::new));


        final Stream<UUID> idsOfPigsStream = Arrays.asList(pigs).stream().map(Pig::getId);
    }


    @Test
    public void createAnimalsWithMapAndFilterBoar() {
        final Pig[] pigs = IntStream.range(0, 10).mapToObj(i -> new Pig(i * 10, randomGender(i))).toArray((Pig[]::new));


        Arrays.asList(pigs).stream().filter(pig -> {
            System.out.println("filter " + pig.getWeight());
            return true;
        }).forEach(pig -> System.out.println("foreach " + pig.getWeight()));
    }

    @Test
    public void createAnimalsWithMapAndGroupByGender() {
        final Pig[] pigs = IntStream.range(0, 10).mapToObj(i -> new Pig(i * 10, randomGender(i))).toArray((Pig[]::new));


        final Map<String, List<Pig>> genderToPigList = Arrays.asList(pigs).stream().collect(Collectors.groupingBy(Pig::getGender));
    }

    @Test
    public void createAnimalsCollect() {
        final Pig[] pigs = IntStream.range(0, 10).mapToObj(i -> new Pig(i * 10, randomGender(i))).toArray((Pig[]::new));


        final List<Pig> collect = Arrays.asList(pigs).stream().collect(Collectors.toList());


        final Map<Boolean, List<Pig>> pigsLessAndMoreThen30Kilos
                = Arrays.asList(pigs).stream().collect(Collectors.partitioningBy(pig -> pig.getWeight() <= 30));

        final Map<String, Double> genderToAverageWeight = Arrays.asList(pigs).stream().collect(Collectors.groupingBy(Pig::getGender, Collectors.averagingDouble(Pig::getWeight)));


        String joined = Arrays.asList(pigs).stream().map(Object::toString).collect(Collectors.joining(", "));

    }

    @Test
    public void createAnimalsWithMapAndLimit() {
        final Pig[] pigs = IntStream.range(0, 10).mapToObj(i -> new Pig(i * 10, randomGender(i))).toArray((Pig[]::new));


        final Optional<Pig> first = Arrays.asList(pigs).stream().limit(5).findFirst();
    }

    @Test
    public void createAnimalsWithMapAndSkip() {
        final Pig[] pigs = IntStream.range(0, 10).mapToObj(i -> new Pig(i * 10, randomGender(i))).toArray((Pig[]::new));


        final Optional<Pig> first = Arrays.asList(pigs).stream().skip(5).findFirst();
        System.out.println(first.get().getWeight());
    }


    @Test
    public void createAnimalsWithMapAndSort() {
        final Pig[] pigs = IntStream.range(0, 10).mapToObj(i -> new Pig(i * 10, randomGender(i))).toArray((Pig[]::new));


        Arrays.asList(pigs).stream().sorted(Comparator.<Pig>naturalOrder());

        Arrays.asList(pigs).stream().sorted(Comparator.comparing(Pig::getWeight));
    }

    @Test
    public void createAnimalsWithMapPeekAndFilter() {
        final Pig[] pigs = IntStream.range(0, 10).mapToObj(i -> new Pig(i * 10, randomGender(i))).toArray((Pig[]::new));


        Arrays.asList(pigs).stream().
                peek(System.out::println).
                filter(pig -> "boar".equalsIgnoreCase(pig.getGender())).
                forEach(System.out::println);
    }


    @Test
    public void createAnimalsWithMapAndOptinal() {
        final Pig[] pigs = IntStream.range(0, 10).mapToObj(i -> new Pig(i * 10, randomGender(i))).toArray((Pig[]::new));


        final Optional<Pig> first = Arrays.asList(pigs).stream().skip(5).findFirst();
        if(first.isPresent())

            System.out.println(first.get().getWeight());

        first.ifPresent(System.out::println);
        final Optional<Pig> maybeBoar = first.filter(pig -> pig.getGender().equalsIgnoreCase("boar"));

        final Optional<Integer> maybeWeightOfFirst = first.map(Pig::getWeight);

        final Pig boar = first.orElse(new Pig(100, "boar"));

        final Pig pig = first.orElseThrow(IllegalStateException::new);

        first.orElseGet(Pig::new);
    }

    @Test
    public void showParallel(){
        final Pig[] pigs = IntStream.range(0, 10).mapToObj(i -> new Pig(i * 10, randomGender(i))).toArray((Pig[]::new));
        Arrays.asList(pigs).parallelStream().forEach(System.out::println);
    }

    @Test
    public void showParallelOrdered(){
        final Pig[] pigs = IntStream.range(0, 10).mapToObj(i -> new Pig(i * 10, randomGender(i))).toArray((Pig[]::new));


        Arrays.asList(pigs).parallelStream().forEachOrdered(System.out::println);

    }


    private String randomGender(int i) {
        return i % 2 == 0 ? "Boar" : "Sow";
    }
}
