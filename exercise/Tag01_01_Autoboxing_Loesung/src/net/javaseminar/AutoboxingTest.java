package net.javaseminar;

import org.junit.Assert;
import org.junit.Test;

public class AutoboxingTest {
	
	

	@Test
	public void vergleichVonIntegerMitInt(){
		Assert.assertTrue(5 <= new Integer(5));
		
	}
	
	@Test
	public void vergleichAufKleiner(){
		Assert.assertFalse(5 < new Integer(5));
	}
	
	@Test
	public void vergleicheVonIntegerMitIntIdentitšt(){
		Assert.assertTrue(5 == new Integer(5));
	}
	
	@Test
	public void vergleicheMitEquals(){
		Assert.assertTrue(new Integer(5).equals(5));
	}
	
	@Test
	public void vergleiche2Integer(){
		Integer fuenf = new Integer(5);
		Integer fuenf2 = new Integer(5);
		Assert.assertFalse(fuenf == fuenf2);
		Assert.assertTrue(fuenf <= fuenf2);
	}
	
	
	@Test
	public void castOderBoxing(){
		Mathe mathe = new Mathe();
		Assert.assertFalse(mathe.tuWas(5));
	}
	
	@Test
	public void vergleichBoolean(){
		Boolean true1= new Boolean(true);
		Boolean true2= new Boolean(true);
		Assert.assertFalse(true1 == true2);
		true1 = Boolean.TRUE;
		true2 = Boolean.TRUE;
		Assert.assertTrue(true1 == true2);
		
		true1 = true; //NICHT: new Boolean(true) SONDERN Boolean.TRUE
		true2 = true; // DITO
		Assert.assertTrue(true1 == true2);
	}
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
}
