package net.fuhrparkservice;

public enum CountryCodes {
	
	DE("Germany"), EN("England"), US("United States of America");
	
	CountryCodes(String fullname){
		this.fullname = fullname;
	}
	
	String fullname;
	
	
	public String getFullname() {
		return fullname;
	}
	
	
}
