package ner.fuhrparkservice;

import static org.junit.Assert.*;
import net.fuhrparkservice.CountryCodes;

import org.junit.Test;

public class TestCountryCodes {
	
	
	@Test
	public void fullNameMustBeGermany(){
		assertEquals("Germany",CountryCodes.DE.getFullname());
	}
	
}
