package net.fuhrparkservice.service;

import java.util.List;

public interface FuhrparkService {
	
	List<String> sucheFahrzeugeMitFabrikat(String fabrikat);

}
