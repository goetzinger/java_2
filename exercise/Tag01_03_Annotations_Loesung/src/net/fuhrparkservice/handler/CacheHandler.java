package net.fuhrparkservice.handler;

import java.lang.reflect.InvocationHandler;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.util.HashMap;

public class CacheHandler implements InvocationHandler {

	private final Object delegate;
	private final HashMap<Object, Object> cachedObjects = new HashMap<Object, Object>();

	public CacheHandler(Object delegate) {
		super();
		this.delegate = delegate;
	}

	@Override
	public Object invoke(Object arg0, Method method, Object[] arguments)
			throws Throwable {
		Method methodAtDelegate = getCorrespondingMethodOfDelegate(method);
		if (methodAtDelegate.isAnnotationPresent(Cache.class)) {
			return aroundCacheMethod(arg0, methodAtDelegate, arguments);
		} else
			return proceeedCallToOrigin(methodAtDelegate, arguments);
	}

	private Method getCorrespondingMethodOfDelegate(Method method) throws SecurityException, NoSuchMethodException {
		return delegate.getClass().getMethod(method.getName(),  method.getParameterTypes());
	}

	private Object proceeedCallToOrigin(Method method, Object[] arguments)
			throws IllegalAccessException, InvocationTargetException {
		return method.invoke(delegate, arguments);
	}

	private Object aroundCacheMethod(Object arg0, Method method,
			Object[] arguments) throws IllegalAccessException,
			InvocationTargetException {
		Object keyToSearchInCache = arguments[0];
		if (cachedObjects.containsKey(keyToSearchInCache))
			return cachedObjects.get(keyToSearchInCache);
		else {
			Object returnValueOfOriginMethodCall = this.proceeedCallToOrigin(
					method, arguments);
			putObjectInCache(returnValueOfOriginMethodCall, keyToSearchInCache);
			return returnValueOfOriginMethodCall;
		}
	}

	private void putObjectInCache(Object returnValueOfOriginMethodCall,
			Object keyToSearchInCache) {
		this.cachedObjects.put(keyToSearchInCache,
				returnValueOfOriginMethodCall);
	}

}
