package net.fuhrparkservice.service;

import java.util.List;

import net.fuhrparkservice.handler.Cache;

public interface FuhrparkService {
	
	@Cache
	List<String> sucheFahrzeugeMitFabrikat(String fabrikat);

}
