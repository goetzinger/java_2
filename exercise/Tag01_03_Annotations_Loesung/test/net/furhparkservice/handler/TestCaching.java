package net.furhparkservice.handler;

import static org.junit.Assert.assertSame;

import java.lang.reflect.Proxy;
import java.util.List;

import net.fuhrparkservice.handler.CacheHandler;
import net.fuhrparkservice.service.FuhrparkService;
import net.fuhrparkservice.service.FuhrparkServiceDefaultImpl;

import org.junit.Before;
import org.junit.Test;

public class TestCaching {

	private FuhrparkService proxiedService;

	@Before
	public void setUpServiceWithProxy(){
		FuhrparkServiceDefaultImpl realService = new FuhrparkServiceDefaultImpl();
		proxiedService = (FuhrparkService) Proxy.newProxyInstance(
				ClassLoader.getSystemClassLoader(),
				new Class[] { FuhrparkService.class }, new CacheHandler(
						realService));
	}

	@Test
	public void callingServiceTwiceShouldResultInSameReturnValue()
			throws InterruptedException {
		List<String> fahrzeugeErsterAufruf = proxiedService.sucheFahrzeugeMitFabrikat("bmw");
		String fahrzeugVomErstenAufruf = fahrzeugeErsterAufruf.get(0);
		Thread.currentThread().sleep(100);
		List<String> fahrzeugeZweiterAufruf = proxiedService.sucheFahrzeugeMitFabrikat("bmw");
		String fahrzeugVomZweitenAufruf = fahrzeugeZweiterAufruf.get(0);
		assertSame(fahrzeugVomErstenAufruf, fahrzeugVomZweitenAufruf);
	}

}
