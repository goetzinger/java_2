package net.javaseminar;

public class Paeckchen {
	
	private Object inhalt;

	public Paeckchen(Object inhalt) {
		super();
		this.inhalt = inhalt;
	}
	
	public Object getInhalt() {
		return inhalt;
	}
}
