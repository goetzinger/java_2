package net.javaseminar;

import java.awt.Color;

import org.junit.Before;

public class PaeckchenTest {

	private Paeckchen schuhPaeckchen;
	private Paeckchen farbPaeckchen;
	private Frau frau;

	@Before
	public void paeckchenErzeugen() {
		schuhPaeckchen = new Paeckchen(new Schuh("Bufallo"));
		farbPaeckchen = new Paeckchen(Color.red);
		frau = new Frau();
	}	
	
	public void frauSollteMannRufen(){
		frau.briefboteKommtMitPaeckchen(farbPaeckchen);
	}
}