package net.javaseminar;

public class GenerischesPaeckchen<T> {
	
	private T inhalt;

	public GenerischesPaeckchen(T inhalt) {
		super();
		this.inhalt = inhalt;
	}
	
	public T getInhalt() {
		return inhalt;
	}
	
	public T tauscheInhalt(T neuerInhalt){
		T alterInhalt = this.inhalt;
		this.inhalt = neuerInhalt;
		return alterInhalt;
	}
}
