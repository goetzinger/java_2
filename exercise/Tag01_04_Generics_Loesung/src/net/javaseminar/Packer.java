package net.javaseminar;

public class Packer {
	
	public <T> GenerischesPaeckchen<T> verpacke(T inhalt){
		GenerischesPaeckchen<T> p = new GenerischesPaeckchen<T>(inhalt);
		return p;
	}

}
