package net.javaseminar;

public class UniversellePacker {
	
	public <T> GenerischesPaeckchen<T> verpacke(T inhalt){
		GenerischesPaeckchen<T> p = new GenerischesPaeckchen<T>(inhalt);
		return p;
	}
	
	 public <T extends Artikel> GenerischesPaeckchen<T> verpacke(T inhalt){
		System.out.println("Verpacke Artikel " + inhalt.getBezeichnung());
		GenerischesPaeckchen<T> p = new GenerischesPaeckchen<T>(inhalt);
		return p;
	}

}
