package net.javaseminar;

import java.awt.Color;
import java.util.HashMap;
import java.util.Map;

import junit.framework.Assert;

import org.junit.Test;

public class PaeckchenTest {

	@Test(expected = ClassCastException.class)
	public void paeckchenErzeugen() {
		Paeckchen p = new Paeckchen("Bitburger");
		Paeckchen p2 = new Paeckchen(Color.red);

		Color roteFarbe = (Color) p2.getInhalt();
		Color blaueFarbe = (Color) p.getInhalt();
	}

	@Test
	public void bierTest() {
		GenerischesPaeckchen<Bier> bierpaeckchen = new GenerischesPaeckchen<Bier>(
				new Bier("Bit"));
		// Kein Typcastnotwendig
		Bier bier = bierpaeckchen.getInhalt();
		new Mann().briefboteKommtMitPaeckchen(bierpaeckchen);
		/*
		 * Geht nicht, da falsch typisiertes Paeckchen new Frau()
		 * .briefboteKommtMitPaeckchen(new GenerischesPaeckchen<Integer>( new
		 * Integer(0)));
		 */
	}

	@Test
	public void testeGenerischeVerpackeMethode() {
		Schuh schuh = new Schuh();
		schuh.setBezeichnung("Meine geilen Pumps");
		UniversellePacker packer = new UniversellePacker();
		GenerischesPaeckchen<Schuh> verpacke = packer.verpacke(schuh);

		Assert.assertEquals(schuh, verpacke.getInhalt());
	}

	@Test
	public void testeGenerischeVerpackeMethodeMitVererbung() {
		Schuh schuh = new Schuh();
		schuh.setBezeichnung("Meine geilen Pumps");
		UniversellePacker packer = new UniversellePacker();
		GenerischesPaeckchen<? extends Artikel> verpacke = (GenerischesPaeckchen<Schuh>) packer
				.verpacke(schuh);
		Artikel inhalt = verpacke.getInhalt();
		System.out.println(inhalt.getBezeichnung());
		// verpacke.tauscheInhalt(new Bier());
		// Laufzeitfehler, obwohl Inhalt korrekterweise ausgetauscht
		// Schuh neuerSchuh = verpacke.getInhalt();
		Assert.assertEquals(schuh, verpacke.getInhalt());

	}

	@Test
	public void schuhMap() {
		Map<String, Schuh> modell2SchuhMap = new HashMap<String, Schuh>();
		modell2SchuhMap.put("Bufallo", new Schuh());
		Schuh schuh = modell2SchuhMap.get("Bufallo");

		Map<String, ? extends Artikel> map2 = modell2SchuhMap;
		modell2SchuhMap.put(null, new Schuh());
	}

	@Test
	public void hashMapGet() {
		Schuh schuh = new Schuh();
		schuh.setBezeichnung("Bufallo");
		Bier bier = new Bier("XY-Bier");
		Map<Schuh, Bier> schuh2BierMap = new HashMap<Schuh, Bier>();
		schuh2BierMap.put(schuh, bier);

		Schuh gleicherSchuh = new Schuh();
		gleicherSchuh.setBezeichnung("Bufallo");

		Assert.assertTrue(schuh.equals(gleicherSchuh));
		System.out.println(schuh.hashCode());
		Assert.assertEquals(schuh.hashCode(), gleicherSchuh.hashCode());
		Assert.assertNotNull(schuh2BierMap.get(gleicherSchuh));
	}

}
