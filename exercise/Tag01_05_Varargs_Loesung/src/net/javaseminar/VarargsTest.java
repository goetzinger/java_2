package net.javaseminar;

import java.util.Date;
import java.util.Locale;

import org.junit.Test;

public class VarargsTest {
	
	@Test
	public void formatTest(){
		Locale.setDefault(Locale.ENGLISH);
		String ergebnis = String.format("Heute ist der %1$te.%1$tm.%1$tY", new Date());
		System.out.println(ergebnis);
		double d = 12344.1212;
		double d1 = 0.2323232;
		System.out.println(String.format("Zahl formatiert %.2f und noch eine %f",d, d1,1.0));
		Object[] params = new Object[2];
		params[0] = d;
		params[1] = d1;
		System.out.println(String.format("Zahl formatiert %.2f und noch eine %f",params));
	}

}
