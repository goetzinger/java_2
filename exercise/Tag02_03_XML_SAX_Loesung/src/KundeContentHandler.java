import java.util.LinkedList;

import org.xml.sax.Attributes;
import org.xml.sax.Locator;
import org.xml.sax.SAXException;
import org.xml.sax.helpers.DefaultHandler;

public class KundeContentHandler extends DefaultHandler {

	private LinkedList<Kunde> kunden;
	private Kunde currentKunde;
	private KundeXMLKindelemente currentKindelement;

	@Override
	public void characters(char[] ch, int start, int length)
			throws SAXException {
		if (currentKunde != null && currentKindelement != null) {
			String s = new String(ch, start, length).trim();
			if (s.length() > 0)
				currentKindelement.setValueToKunde(currentKunde, s);
		}
	}

	@Override
	public void endDocument() throws SAXException {
		for (Kunde gefunden : kunden) {
			System.out.println(gefunden);
		}

	}

	@Override
	public void endElement(String uri, String localName, String qName)
			throws SAXException {
		if (qName.equalsIgnoreCase("kunde")) {
			this.kunden.add(currentKunde);
			currentKunde = null;
		} else if (currentKindelement != null) {
			currentKindelement = null;
		}

	}

	@Override
	public void startDocument() throws SAXException {
		this.kunden = new LinkedList<Kunde>();

	}

	@Override
	public void startElement(String uri, String localName, String qName,
			Attributes atts) throws SAXException {
		if (qName.equalsIgnoreCase("kunde")) {
			currentKunde = new Kunde();
			currentKunde.gs = atts.getValue("gs");
		} else {
			for (KundeXMLKindelemente kindelement : KundeXMLKindelemente
					.values()) {
				if (kindelement.isKindelelemnt(qName))
					this.currentKindelement = kindelement;
			}
		}
	}
	
}
