import java.lang.reflect.Field;

public enum KundeXMLKindelemente {

	FIRMA("firma"), Bereich("bereich"), STRPFACH("strpfach"), PLZ("plz"), ORT(
			"ort");
	
	private String elementName;

	private KundeXMLKindelemente(String elementName) {
		this.elementName = elementName;
	}
	
	public boolean isKindelelemnt(String qName){
		return this.elementName.equalsIgnoreCase(qName);
	}
	
	public void setValueToKunde(Kunde k, String s){
		try {
			Field declaredField = k.getClass().getDeclaredField(elementName);
			declaredField.setAccessible(true);
			declaredField.set(k, s);
		} catch (SecurityException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (NoSuchFieldException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IllegalArgumentException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IllegalAccessException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

}
