package net.javaseminar;

import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.Properties;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import javax.xml.transform.OutputKeys;
import javax.xml.transform.Transformer;
import javax.xml.transform.TransformerConfigurationException;
import javax.xml.transform.TransformerException;
import javax.xml.transform.TransformerFactory;
import javax.xml.transform.TransformerFactoryConfigurationError;
import javax.xml.transform.dom.DOMSource;
import javax.xml.transform.stream.StreamResult;

import org.w3c.dom.Attr;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.NodeList;
import org.xml.sax.SAXException;

public class Application {

	/**
	 * @param args
	 */
	public static void main(String[] args) {

		Kunde k = new Kunde();
		k.bereich = "Finanzen";
		k.firma = "My Big Business";
		k.gs = "HH";
		k.ort = "Hamburg";
		k.strpfach = "Hamburg Hafen 1";
		k.plz = "12345";
		try {
			DocumentBuilder newDocumentBuilder = DocumentBuilderFactory
					.newInstance().newDocumentBuilder();
			Document document = newDocumentBuilder.parse("src/kunde.xml");
			Element rootElement = document.getDocumentElement();
			NodeList elementsByTagName = rootElement.getElementsByTagName("kunde");
			Document doc = newDocumentBuilder.newDocument();
			Element kundeElement = doc.createElement("kunde");
			doc.appendChild(kundeElement);

			Attr gsAttr = doc.createAttribute("gs");
			gsAttr.setNodeValue(k.gs);
			kundeElement.getAttributes().setNamedItem(gsAttr);

			Element firmaElement = doc.createElement("firma");
			firmaElement.appendChild(doc.createTextNode(k.firma));
			kundeElement.appendChild(firmaElement);

			Element bereichElement = doc.createElement("bereich");
			bereichElement.appendChild(doc.createTextNode(k.bereich));
			kundeElement.appendChild(bereichElement);

			Element strElement = doc.createElement("strpfach");
			strElement.appendChild(doc.createTextNode(k.strpfach));
			kundeElement.appendChild(strElement);

			Element plzElement = doc.createElement("plz");
			plzElement.appendChild(doc.createTextNode(k.plz));
			kundeElement.appendChild(plzElement);

			Element ortElement = doc.createElement("ort");
			ortElement.appendChild(doc.createTextNode(k.ort));
			kundeElement.appendChild(ortElement);

			TransformerFactory tf = TransformerFactory.newInstance();
			Transformer t = tf.newTransformer();
			Properties p = t.getOutputProperties();
			p.list(System.out);
			p.put(OutputKeys.INDENT, "yes");
			p.put("encoding", "iso-8859-1");
			p.put("{http://xml.apache.org/xslt}indent-amount", "2");
			p.list(System.out);
			t.setOutputProperties(p);

			FileOutputStream fos = new FileOutputStream("kunde.xml");
			t.transform(new DOMSource(doc), new StreamResult(fos));
			fos.close();
		} catch (TransformerConfigurationException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (TransformerFactoryConfigurationError e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (FileNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (TransformerException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (ParserConfigurationException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (SAXException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

	}

}
