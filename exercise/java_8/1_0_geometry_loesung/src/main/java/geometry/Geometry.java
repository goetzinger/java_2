package geometry;

import org.apache.commons.lang3.tuple.Pair;

public interface Geometry {

    Pair<Integer, Integer> getCenter();

    void setCenter(int x, int y);



}
