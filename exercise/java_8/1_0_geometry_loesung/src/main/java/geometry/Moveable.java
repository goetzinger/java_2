package geometry;

import org.apache.commons.lang3.tuple.Pair;

/**
 * Created by goetzingert on 01.04.16.
 */
public interface Moveable extends Geometry{

    default void moveLeft(){
        final Pair<Integer, Integer> center = getCenter();
        setCenter(center.getLeft()-1, center.getRight());
    }

    default void moveRight(){
        final Pair<Integer, Integer> center = getCenter();
        setCenter(center.getLeft()+1, center.getRight());
    }

    default void moveUp(){
        final Pair<Integer, Integer> center = getCenter();
        setCenter(center.getLeft(), center.getRight()+1);
    }

    default void moveDown(){
        final Pair<Integer, Integer> center = getCenter();
        setCenter(center.getLeft(), center.getRight()-1);
    }
}
