package geometry;

import org.apache.commons.lang3.tuple.ImmutablePair;
import org.apache.commons.lang3.tuple.MutablePair;
import org.apache.commons.lang3.tuple.Pair;

/**
 * Created by goetzingert on 01.04.16.
 */
public class Point implements Moveable {

    private MutablePair<Integer, Integer> center = new MutablePair<>(0,0);
    @Override
    public Pair<Integer, Integer> getCenter() {
        return new ImmutablePair<>(center.getLeft(),center.getRight());
    }

    @Override
    public void setCenter(int x, int y)  {
        center.setLeft(x);
        center.setRight(y);

    }
}
