package geometry;

import org.apache.commons.lang3.tuple.Pair;

/**
 * Created by goetzingert on 01.04.16.
 */
public interface Bordered {

  default void setCenter(int x, int y){
      if(canMove(x,y))
          moveTo(x,y);
  }

    void moveTo(int x, int y);

    boolean canMove(int x, int y);

}
