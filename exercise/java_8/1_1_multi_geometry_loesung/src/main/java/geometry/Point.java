package geometry;

import org.apache.commons.lang3.tuple.ImmutablePair;
import org.apache.commons.lang3.tuple.MutablePair;
import org.apache.commons.lang3.tuple.Pair;

public class Point implements Moveable,Bordered {

    private final int minX;
    private final int maxX;
    private final int minY;
    private final int maxY;

    public Point(int minX, int maxX, int minY, int maxY){

        this.minX = minX;
        this.maxX = maxX;
        this.minY = minY;
        this.maxY = maxY;
    }

    private MutablePair<Integer, Integer> center = new MutablePair<>(0,0);
    @Override
    public Pair<Integer, Integer> getCenter() {
        return new ImmutablePair<>(center.getLeft(),center.getRight());
    }

    @Override
    public void setCenter(int x, int y) {
        Bordered.super.setCenter(x,y);
    }


    @Override
    public void moveTo(int x, int y) {
        center.setLeft(x);
        center.setLeft(y);
    }

    @Override
    public boolean canMove(int x, int y) {
        return (minX <= x) && (x <= maxX) && (minY <= y) && (y <= maxY);
    }

}
