package net.fuhrparkservice.model;

import java.time.ZoneId;
import java.util.HashSet;
import java.util.Set;

public class Filiale extends AbstractBusinessObject {

	private String ort;
	private Set<Fahrzeug> fahrzeuge = new HashSet<Fahrzeug>();

	private ZoneId timeZone;

	public Filiale() {
	}

	public Filiale(String ort) {
		this.ort = ort;
	}

	public String getOrt() {
		return ort;
	}

	public void setOrt(String ort) {
		this.ort = ort;
	}

	public Set<Fahrzeug> getFahrzeuge() {
		return fahrzeuge;
	}

	public void setFahrzeuge(Set<Fahrzeug> fahrzeuge) {
		this.fahrzeuge = fahrzeuge;
	}


	public ZoneId getTimeZone() {
		return timeZone;
	}

	public void setTimeZone(ZoneId timeZone) {
		this.timeZone = timeZone;
	}

	@Override
	public String toString() {
		return this.ort;
	}

}
