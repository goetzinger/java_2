/**
 * 
 */
package net.fuhrparkservice.model;

import java.util.ArrayList;
import java.util.List;
import java.util.UUID;

/**
 * @author Thomas G&ouml;tzinger
 * 
 */
public class Kunde extends Nutzer {

	private String kundennummer;
	private boolean inTopState;
	private List<Reservierung> reservierungen = new ArrayList<Reservierung>();

	public List<Reservierung> getReservierungen() {
		return reservierungen;
	}

	public void setReservierungen(List<Reservierung> reservierungen) {
		this.reservierungen = reservierungen;
	}

	public Kunde() {
		super();
		this.kundennummer = UUID.randomUUID().toString();
	}

	/**
	 * Konstruktor der Klasse
	 * 
	 * @param p
	 *            Personeninformation des Kunden
	 */
	public Kunde(Person p) {
		super(p);
		this.kundennummer = UUID.randomUUID().toString();
	}

	@Override
	public boolean equals(Object obj) {
		if (obj instanceof Kunde) {
			Kunde other = (Kunde) obj;
			return this.kundennummer.equals(other.kundennummer);
		}
		return false;
	}

	public String getKundennummer() {
		return kundennummer;
	}

	public void setKundennummer(String kundennummer) {
		this.kundennummer = kundennummer;
	}

	public boolean isInTopState() {
		return inTopState;
	}

	public void setInTopState(boolean inTopState) {
		this.inTopState = inTopState;
	}

	@Override
	public int hashCode() {
		return kundennummer.hashCode();
	}

}
