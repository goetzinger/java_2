package net.fuhrparkservice.model;

public class Nutzer extends AbstractBusinessObject {

	private Person person;

	public Nutzer() {
		super();
	}

	public Nutzer( Person person) {
		this.person = person;
	}

	public Person getPerson() {
		return person;
	}

	public void setPerson(Person person) {
		this.person = person;
	}


}
