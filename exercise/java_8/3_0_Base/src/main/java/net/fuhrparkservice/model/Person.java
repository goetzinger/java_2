package net.fuhrparkservice.model;



/**
 * Entity implementation class for Entity: Person
 * 
 */
public class Person extends AbstractBusinessObject {

	private String vorname;
	private String nachname;

	public Person() {
		super();
	}
	

	public Person(String vorname, String nachname) {
		this.vorname = vorname;
		this.nachname = nachname;
	}


	public String getVorname() {
		return vorname;
	}

	public void setVorname(String vorname) {
		this.vorname = vorname;
	}

	public String getNachname() {
		return nachname;
	}

	public void setNachname(String nachname) {
		this.nachname = nachname;
	}

}
