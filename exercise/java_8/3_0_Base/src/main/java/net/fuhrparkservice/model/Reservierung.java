package net.fuhrparkservice.model;

import java.util.Calendar;
import java.util.GregorianCalendar;

public class Reservierung extends AbstractBusinessObject {

	private Fahrzeug fahrzeug;
	private Calendar startZeit;
	private Calendar rueckgabeZeit;
	private Filiale startFiliale;
	private Filiale rueckgabeFiliale;
	private float preis;

	public Reservierung() {
	}

	public Reservierung(Fahrzeug fahrzeugItem, Filiale startFiliale,
			Filiale rueckgabeFiliale, GregorianCalendar start,
			GregorianCalendar ende, float preis) {
		super();
		setFahrzeug(fahrzeugItem);
		setStartFiliale(startFiliale);
		setRueckgabeFiliale(rueckgabeFiliale);
		setStartZeit(start);
		setRueckgabeZeit(ende);
		setPreis(preis);
	}

	public float getPreis() {
		return preis;
	}

	public void setPreis(float preis) {
		this.preis = preis;
	}

	public Fahrzeug getFahrzeug() {
		return fahrzeug;
	}

	public void setFahrzeug(Fahrzeug fahrzeug) {
		this.fahrzeug = fahrzeug;
	}

	public Calendar getStartZeit() {
		return startZeit;
	}

	public void setStartZeit(Calendar startZeit) {
		this.startZeit = startZeit;
	}

	public Calendar getRueckgabeZeit() {
		return rueckgabeZeit;
	}

	public void setRueckgabeZeit(Calendar rueckgabeZeit) {
		this.rueckgabeZeit = rueckgabeZeit;
	}

	public Filiale getStartFiliale() {
		return startFiliale;
	}

	public void setStartFiliale(Filiale startFiliale) {
		this.startFiliale = startFiliale;
	}

	public Filiale getRueckgabeFiliale() {
		return rueckgabeFiliale;
	}

	public void setRueckgabeFiliale(Filiale rueckgabeFiliale) {
		this.rueckgabeFiliale = rueckgabeFiliale;
	}

}
