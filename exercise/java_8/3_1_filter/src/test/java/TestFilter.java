import static org.junit.Assert.assertEquals;
import net.fuhrparkservice.model.Fahrzeug;
import net.fuhrparkservice.model.FahrzeugType;
import net.fuhrparkservice.model.Filiale;
import net.fuhrparkservice.model.LKW;
import net.fuhrparkservice.model.Model;
import net.fuhrparkservice.model.Nutzer;
import net.fuhrparkservice.model.PKW;
import net.fuhrparkservice.model.Person;

import org.junit.Before;
import org.junit.Test;

import java.util.ArrayList;
import java.util.List;

public class TestFilter {

	List<FahrzeugType> fahrzeuge = new ArrayList<>();

	@Before
	public void setUp()  {
		
		FahrzeugType fahrzeug = new PKW(new Model("VW", "Golf"), 120, 200, 2);
		FahrzeugType fahrzeug2 = new LKW( new Model("Mercedes", "10to"), 120,
				200, 10000);
		FahrzeugType fahrzeug3 = new PKW(new Model("BMW", "323"), 150, 220, 4);
		fahrzeuge.add(fahrzeug);
		fahrzeuge.add(fahrzeug2);
		fahrzeuge.add(fahrzeug3);

	}

	@Test
	public void testFilterForFahrzeugMoreThen130PS() {
        //filter fahrzeuge > 130 ps
		assertEquals(1, 0);
	}
}
