import net.fuhrparkservice.model.*;
import org.junit.Before;
import org.junit.Test;

import java.util.ArrayList;
import java.util.List;

import static org.junit.Assert.assertEquals;

public class TestPaging {

	List<Nutzer> nutzer = new ArrayList<>();

	@Before
	public void setUp()  {

		nutzer.add(new Nutzer(new Person("Hans", "Mustermann")));
		nutzer.add(new Nutzer(new Person("Franz", "M�ller")));
		nutzer.add(new Nutzer(new Person("Herbert", "Schmitt")));
		nutzer.add(new Nutzer(new Person("Ingo", "Meyer")));
		nutzer.add(new Nutzer(new Person("Mathias", "Mayer")));
		nutzer.add(new Nutzer(new Person("Michael", "Anst�dt")));
		nutzer.add(new Nutzer(new Person("Ralf", "Gross")));


	}

	@Test
	public void pageOverNuterArrayWithPageSize3() {

		//create stream with 3 elements and increment firstResult by page size
		List<Nutzer> firstPage = null;
		List<Nutzer> sndPage = null;
		List<Nutzer> thirdPage = null;

		assertEquals(3, firstPage.size());
		assertEquals(3, sndPage.size());
		assertEquals(1,thirdPage.size());
	}
}
