import net.fuhrparkservice.model.*;
import org.junit.Before;
import org.junit.Test;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

import static org.junit.Assert.assertEquals;

public class TestPaging {

	public static final int PAGE_SIZE = 3;
	List<Nutzer> nutzer = new ArrayList<>();

	@Before
	public void setUp()  {

		nutzer.add(new Nutzer(new Person("Hans", "Mustermann")));
		nutzer.add(new Nutzer(new Person("Franz", "M�ller")));
		nutzer.add(new Nutzer(new Person("Herbert", "Schmitt")));
		nutzer.add(new Nutzer(new Person("Ingo", "Meyer")));
		nutzer.add(new Nutzer(new Person("Mathias", "Mayer")));
		nutzer.add(new Nutzer(new Person("Michael", "Anstaedt")));
		nutzer.add(new Nutzer(new Person("Ralf", "Gross")));


	}

	@Test
	public void pageOverNuterArrayWithPageSize3() {

		//create stream with 3 elements and increment firstResult by page size
		List<Nutzer> firstPage =  nutzer.stream().limit(PAGE_SIZE).collect(Collectors.toList());

		List<Nutzer> sndPage = nutzer.stream().skip(PAGE_SIZE).limit(PAGE_SIZE).collect(Collectors.toList());;
		List<Nutzer> thirdPage = nutzer.stream().skip(PAGE_SIZE*2).limit(PAGE_SIZE).collect(Collectors.toList());;;

		assertEquals(PAGE_SIZE, firstPage.size());
		assertEquals(PAGE_SIZE, sndPage.size());
		assertEquals(1,thirdPage.size());
	}
}
