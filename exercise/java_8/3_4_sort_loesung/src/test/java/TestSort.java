import net.fuhrparkservice.model.FahrzeugType;
import net.fuhrparkservice.model.LKW;
import net.fuhrparkservice.model.Model;
import net.fuhrparkservice.model.PKW;
import org.junit.Before;
import org.junit.Test;

import java.util.ArrayList;
import java.util.Comparator;
import java.util.List;
import java.util.Optional;

import static org.junit.Assert.assertEquals;

public class TestSort {

	List<FahrzeugType> fahrzeuge = new ArrayList<>();

	@Before
	public void setUp()  {
		
		FahrzeugType fahrzeug = new PKW(new Model("VW", "Golf"), 120, 200, 2);
		FahrzeugType fahrzeug2 = new LKW( new Model("Mercedes", "10to"), 120,
				200, 10000);
		FahrzeugType fahrzeug3 = new PKW(new Model("BMW", "323"), 150, 220, 4);
		fahrzeuge.add(fahrzeug);
		fahrzeuge.add(fahrzeug2);
		fahrzeuge.add(fahrzeug3);

	}

	@Test
	public void testSortFahrzeugTypeByModelAscendingSimple() {
		final Optional<FahrzeugType> first = fahrzeuge.stream().sorted((o1, o2) -> o1.getModel().getModell().compareTo(o2.getModel().getModell())).findFirst();
		assertEquals("10to", first.get().getModel().getModell());
	}

	@Test
	public void testSortFahrzeugTypeByModelAscending() {
		final Optional<FahrzeugType> first = fahrzeuge.stream().sorted(Comparator.comparing((FahrzeugType fz) -> fz.getModel(), Comparator.comparing((Model m) -> m.getModell()))).findFirst();
		assertEquals("10to",first.get().getModel().getModell());
	}
}
