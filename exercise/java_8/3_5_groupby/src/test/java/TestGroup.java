import net.fuhrparkservice.model.*;
import org.junit.Before;
import org.junit.Test;

import java.util.ArrayList;
import java.util.GregorianCalendar;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

import static org.junit.Assert.assertEquals;

public class TestGroup {

    private Fahrzeug fahrzeugItem;
    private List<Kunde> kunden = new ArrayList<>();
@Before
    public void setUp() throws Exception {
        FahrzeugType fahrzeug = new PKW(new Model("VW", "Golf"), 120, 200, 2);
        FahrzeugType fahrzeug2 = new LKW(new Model("Mercedes", "10to"), 120,
                200, 10000);
        FahrzeugType fahrzeug3 = new PKW(new Model("BMW", "323"), 150, 220, 4);
        Filiale filiale = new Filiale("Muenchen");
        fahrzeugItem = new Fahrzeug(filiale, fahrzeug);
        Filiale stuttgart = new Filiale("Stuttgart");
        fahrzeugItem.setStandort(stuttgart);
        Fahrzeug fahrzeugItem2 = new Fahrzeug(stuttgart, fahrzeug3);
        Kunde kunde = new Kunde(new Person("Hans", "Mustermann"));
        kunden.add(kunde);
        Kunde kunde2 = new Kunde(new Person("Franz", "M�ller"));
        kunden.add(kunde);
        this.createReservierung(kunde, fahrzeugItem, stuttgart, 100,
                new GregorianCalendar(2007, 12, 18, 12, 0),
                new GregorianCalendar(2007, 12, 19, 12, 0));
        this.createReservierung(kunde2, fahrzeugItem, filiale, 110,
                new GregorianCalendar(2008, 11, 22, 12, 0),
                new GregorianCalendar(2008, 11, 23, 18, 0));
        this.createReservierung(kunde, fahrzeugItem2, stuttgart, 220,
                new GregorianCalendar(2008, 07, 12, 12, 0),
                new GregorianCalendar(2008, 07, 14, 12, 0));
        this.createReservierung(kunde2, fahrzeugItem2, stuttgart, 330,
                new GregorianCalendar(2007, 04, 30, 12, 0),
                new GregorianCalendar(2007, 05, 02, 12, 0));
        this.createReservierung(kunde, fahrzeugItem2, filiale, 440,
                new GregorianCalendar(2007, 10, 01, 12, 0),
                new GregorianCalendar(2007, 10, 04, 18, 0));

    }

    private void createReservierung(Kunde kunde, Fahrzeug fahrzeug,
                                    Filiale filiale, float preis, GregorianCalendar start,
                                    GregorianCalendar ende) {
        Reservierung res = new Reservierung(fahrzeug, filiale, filiale, start,
                ende, preis);
        kunde.getReservierungen().add(res);
    }

    @Test
    public void groupReservierungByFahrzeugModel() {

    }

    @Test
    public void splitKundenMitMehrUndWenigerAls200EuroInReservierungen() {
        Map<Boolean, List<Kunde>> kudenMitMehrAls200EuroReservierungen = null;
        assertEquals(2, kudenMitMehrAls200EuroReservierungen.get(true).size());
    }
}
