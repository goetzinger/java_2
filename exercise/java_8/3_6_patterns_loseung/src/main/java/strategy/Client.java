package strategy;

/**
 * Created by goetzingert on 03.04.16.
 */
public class Client {

    public static void main(String[] args) {
        Context context = new Context();
        context.setStrategy(new SomeStrategy());
        context.doSome();
    }
}
