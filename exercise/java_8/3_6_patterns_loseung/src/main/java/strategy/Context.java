package strategy;

/**
 * Created by goetzingert on 03.04.16.
 */
public class Context {


    private Strategy strategy;

    void doSome(){
        strategy.perform();
    }


    void setStrategy(Strategy strategy) {
        this.strategy = strategy;
    }
}
