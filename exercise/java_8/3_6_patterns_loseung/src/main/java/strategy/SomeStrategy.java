package strategy;

/**
 * Created by goetzingert on 03.04.16.
 */
public class SomeStrategy implements Strategy {
    public void perform() {
        System.out.println("Some Task");
    }
}
