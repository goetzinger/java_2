package strategy;

/**
 * Created by goetzingert on 03.04.16.
 */
public interface Strategy {

    public void perform();

}
