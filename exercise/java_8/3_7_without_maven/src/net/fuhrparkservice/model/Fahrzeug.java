package net.fuhrparkservice.model;

import java.util.ArrayList;
import java.util.List;

public class Fahrzeug extends AbstractBusinessObject{

	private FahrzeugType type;
	private Filiale standort;
	private List<Filiale> standortHistory = new ArrayList<Filiale>();

	public List<Filiale> getStandortHistory() {
		return standortHistory;
	}

	public void setStandortHistory(List<Filiale> standortHistory) {
		this.standortHistory = standortHistory;
	}

	public Fahrzeug() {
	}

	public Fahrzeug(Filiale standort, FahrzeugType type) {
		setStandort(standort);
		this.type = type;
	}

	public void setType(FahrzeugType type) {
		this.type = type;
	}

	public FahrzeugType getType() {
		return type;
	}

	public void setStandort(Filiale standort) {
		standort.getFahrzeuge().add(this);
		if (this.standort != null)
		{
			this.standortHistory.add(this.standort);
			this.standort.getFahrzeuge().remove(this);
		}
		this.standort = standort;
		
	}

	public Filiale getStandort() {
		return standort;
	}
}
