package net.fuhrparkservice.model;

public class FahrzeugType extends AbstractBusinessObject {

	private Model model;
	private long ps = 100;
	private long maxKmH;

	public FahrzeugType() {
	}

	public FahrzeugType(Model model, long ps, long maxKmH) {
		super();
		this.model = model;
		this.ps = ps;
		this.maxKmH = maxKmH;
	}

	public long getPs() {
		return ps;
	}

	public void setPs(long ps) {
		this.ps = ps;
	}

	public long getMaxKmH() {
		return maxKmH;
	}

	public void setMaxKmH(long maxKmH) {
		this.maxKmH = maxKmH;
	}

	@Override
	public boolean equals(Object obj) {
		if (obj != null && obj instanceof FahrzeugType) {
			FahrzeugType other = (FahrzeugType) obj;
			return this.getModel().equals(other.getModel());
		}
		return false;
	}

	@Override
	public int hashCode() {
		return getModel().hashCode();
	}

	@Override
	public String toString() {
		return getModel().toString();
	}

	public void setModel(Model model) {
		this.model = model;
	}

	public Model getModel() {
		return model;
	}

}
