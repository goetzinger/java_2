package java8seminar.reservation;

import net.fuhrparkservice.model.Kunde;
import net.fuhrparkservice.model.Reservierung;

import javax.script.Invocable;
import javax.script.ScriptEngine;
import javax.script.ScriptEngineManager;
import javax.script.ScriptException;
import java.io.InputStreamReader;
import java.time.*;
import java.util.Calendar;
import java.util.TimeZone;

public class ReservationService {


    public double calculatePrice(Reservierung res, Kunde kunde) throws ScriptException, NoSuchMethodException {
        ScriptEngine engine = new ScriptEngineManager().getEngineByName("nashorn");
        engine.eval(new InputStreamReader(this.getClass().getResourceAsStream("/pricecalculator.js")));

        Invocable invocable = (Invocable) engine;


        Double price = (Double) invocable.invokeFunction("calculatePrice",res, kunde);
        return price;
    }
}
