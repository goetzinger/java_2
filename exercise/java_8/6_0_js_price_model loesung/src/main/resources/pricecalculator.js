var calculatePrice = function (res, kunde) {
    var days = calculateDays(res);
    return days * fabrikatPreisAbbildung[res.fahrzeug.fabrikat](res.fahrzeug.model);

}

var fabrikatPreisAbbildung= {'VW': function(arg){return 10;}};
    fabrikatPreisAbbildung= {'BMW': function(model){return if(model === '323') return 25; else return 30;}};

var calculateDays = function (res) {
    var imports = new JavaImporter(java.time);
    with (imports) {
        var start = res.startZeit.toInstant();
        var end = res.rueckgabeZeit.toInstant();


        var from = ZonedDateTime.ofInstant(start, res.startFiliale.timeZone);
        var to = ZonedDateTime.ofInstant(end, res.rueckgabeFiliale.timeZone);


        var period = Period.between(from.toLocalDate(),to.toLocalDate());
        return period.days;
    }


}