import java8seminar.reservation.ReservationService;
import net.fuhrparkservice.model.Filiale;
import net.fuhrparkservice.model.Kunde;
import net.fuhrparkservice.model.Reservierung;
import org.junit.Assert;
import org.junit.Test;

import javax.script.ScriptException;
import java.time.ZoneId;
import java.util.Calendar;

/**
 * Created by goetzingert on 01.04.16.
 */
public class ReservationServiceTest {

    @Test
    public void happyPath() throws ScriptException, NoSuchMethodException {
        Reservierung res = new Reservierung();
        final Calendar startTime = Calendar.getInstance();
        startTime.set(2016, 4, 16);
        res.setStartZeit(startTime);
        final Calendar endTime = Calendar.getInstance();
        endTime.set(2016, 4, 18);
        res.setRueckgabeZeit(endTime);
        Filiale ortDerAusleihe = new Filiale();
        ortDerAusleihe.setTimeZone(ZoneId.systemDefault());
        res.setStartFiliale(ortDerAusleihe);
        res.setRueckgabeFiliale(ortDerAusleihe);
        final double price = new ReservationService().calculatePrice(res, new Kunde());
        Assert.assertEquals(20.0, price, 1.0);

    }
}
