package jpa_setup;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;

import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;

public class DBTest {
	
	private static EntityManagerFactory managerFactory;

	private EntityManager manager;

	@Before
	public void setUpEM() throws Exception {
		managerFactory = Persistence.createEntityManagerFactory("projectUnit");
		manager = managerFactory.createEntityManager();
		manager.isOpen();
	}
	
	@Test
	public void happy() {
		manager.getTransaction().begin();
		manager.persist(new Article());
		manager.getTransaction().commit();
	}

}
